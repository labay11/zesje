import os
import sys

sys.path.append(os.getcwd())
from zesje import app  # noqa: E402


app.run()
